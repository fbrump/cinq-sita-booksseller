"use strict";

angular.module('booksSeller').controller('BookCtrl', ['$scope', 'resourceBooks', 'apiBooks', '$routeParams',
	function ($scope, resourceBooks, apiBooks, $routeParams, $http) {
		$scope.books = [];
		$scope.message = '';
		$scope.modelEdit = {
			formBook: {},
			show: false
		};
		
		$scope.clickToEdit = function (item) {
			console.log(item);
			$scope.modelEdit.formBook = angular.copy(item);
			_showFormEdit(true);
		};

		$scope.clickComeBack = function () {
			// body...
			$scope.modelEdit.formBook = {};
			_showFormEdit(false);
		};

		$scope.submit = function () {
			// body...
			console.log('submit');
			var _form = $scope.modelEdit.formBook;
			if (_form.name.length == 0 || _form.name == null)
				$scope.message = 'Name is field required.'
			else{
				//apiBooks.update(_form)
				resourceBooks.update({
					bookId: _form.id
				}, _form, function (data) {
					$scope.message = 'Book ' + data.name + ' updated succesefuly!';
					$scope.clickComeBack();
				}, function (data) {
					console.info(data);
					$scope.message = 'Happened some error when updated booke ';
				});
			}
		};

		var _showFormEdit = function (isShow) {
			$scope.modelEdit.show = isShow;
		};

		var _listBooks = function () {
			resourceBooks.query(function (books) {
				$scope.books = books;
			}, function (error) {
				console.log(error);
			});	
		};

		_listBooks();

}]);