﻿"use strict";
// app/app.js

angular.module('booksSeller', ['ngResource', 'ngRoute', 'BooksService'])
.config(['$locationProvider', '$routeProvider',
	function ($locationProvider, $routeProvider) {
	
	$locationProvider.html5Mode(true);

	$routeProvider.otherwise({ redirectTo: '/book' })
}]);