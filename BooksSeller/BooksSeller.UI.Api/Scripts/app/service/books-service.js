"use strict";

angular.module('BooksService', ['ngResource'])
.factory('resourceBooks', ['$resource',
	function ($resource) {
		return $resource('api/books/:bookId', null, {
			update: {
				method: 'PUT'
			}
		});
}])
.factory('apiBooks', ['resourceBooks', '$q', function (resourceBooks, $q) {
	var service = {};

	service.update = function (book) {
		console.log(book);
		var deferred = $q.defer();
		var promise = deferred.promise;
		var resolvedValue;

		resourceBooks.update({
				bookId: book.id
			}, book);
		promise.then = function () {
					var _um = {
						message: 'Book ' + book.name + ' updated succesefuly!',
						inclusion: false
					};
				};
		promise.catch = function (error) {
			console.error(error);
					var _dois = {
						message: 'Happened some error when updated booke ' + book.name
					};
		};
		return promise; 
		// resourceBooks.update({
		// 		bookId: book.id
		// 	}, book, null, null);

		// return $q(function (resolve, reject) {
		// 	if (book.id){
		// 		resourceBooks.update({
		// 			bookId: book.id
		// 		}, book, function () {
		// 			resolve({
		// 				message: 'Book ' + book.name + ' updated succesefuly!',
		// 				inclusion: false
		// 			});
		// 		}, function (error) {
		// 			console.error(error);
		// 			reject({
		// 				message: 'Happened some error when updated booke ' + book.name
		// 			});
		// 		});
		// 	}
		// })
	}

	return service;
}])