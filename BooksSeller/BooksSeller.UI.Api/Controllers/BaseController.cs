﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using BooksSeller.BLL.Factory;
using BooksSeller.BLL.Interface;

namespace BooksSeller.UI.Api.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class BaseController<T> : ApiController
    {
        #region[ Shared Property ]
        /// <summary>
        /// The service privatre.
        /// </summary>
        private T _service;

        /// <summary>
        /// Gets the service.
        /// </summary>
        /// <value>
        /// The service.
        /// </value>
        protected T _Service
        {
            get
            {
                if (this._service != null)
                    this._service = FactoryService<T>.GetImplementation();
                return this._service;
            }
        }
        #endregion

        #region [ Constructor ]
        /// <summary>
        /// Initializes a new instance of the <see cref="BaseController{T}"/> class.
        /// </summary>
        public BaseController()
        {
            _service =  FactoryService<T>.GetImplementation();

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseController{T}"/> class.
        /// </summary>
        /// <param name="iService">The interface of service.</param>
        public BaseController(T iService)
        {
            this._service = iService;
        }

        #endregion
    }
}