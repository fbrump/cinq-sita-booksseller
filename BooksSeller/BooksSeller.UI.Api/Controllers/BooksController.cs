﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BooksSeller.BE.DTO;
using BooksSeller.BLL.Factory;
using BooksSeller.BLL.Interface;

namespace BooksSeller.UI.Api.Controllers
{
    public class BooksController : BaseController<IBookService>
    {
        #region [ Constructor ]

        /// <summary>
        /// Initializes a new instance of the <see cref="BooksController"/> class.
        /// </summary>
        public BooksController()
            : base()
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="BooksController"/> class.
        /// </summary>
        /// <param name="iService"></param>
        public BooksController(IBookService iService)
            : base(iService)
        { }
        #endregion

        #region [ Actions ]
        

        // GET: api/Books
        public IEnumerable<BookDTO> Get()
        {
            return base._Service.GetAll();
        }

        // GET: api/Books/5
        public BookDTO Get(int id)
        {
            return _Service.Get(id);
        }

        // POST: api/Books
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Books/5
        public void Put(int id, [FromBody]BookDTO value)
        {
            var _item = base._Service.Get(id);
            if (_item != null)
            {
                base._Service.Update(value);
            }
            else
            {
                throw new Exception("The book not find");
            }
        }

        // DELETE: api/Books/5
        public void Delete(int id)
        {
        }

        #endregion
    }
}
