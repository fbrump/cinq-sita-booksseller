﻿using System.Web;
using System.Web.Optimization;

namespace BooksSeller.UI.Api
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/appAngular").Include(
                      "~/Scripts/lib/angular.min.js",
                      "~/Scripts/lib/angular-resource.min.js",
                      "~/Scripts/lib/angular-route.min.js",
                      "~/Scripts/lib/angular-cookies.min.js",
                      "~/Scripts/lib/angular-animate.min.js",
                      "~/Scripts/app/app.js",
                      "~/Scripts/app/value/config-values.js",
                      "~/Scripts/app/service/books-service.js",
                      "~/Scripts/app/controller/books-controller.js"));
        }
    }
}
