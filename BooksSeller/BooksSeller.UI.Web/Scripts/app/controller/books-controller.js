"use strict";

angular.module('booksSeller').controller('BookCtrl', ['$scope', 'resourceBooks', '$routeParams', '$http',
	function ($scope, resourceBooks, $routeParams, $http) {
		$scope.books = [];

		$scope.books.push({'id':1, 'name': 'Steve Jobs', 'description': null});
		$scope.books.push({'id':2, 'name': 'The children of the Húrin', 'description': 'That book tales about expirienc of the yang boy...'});
		$scope.books.push({'id':3, 'name': 'Bible', 'description': null});

		let url = 'http://localhost:49700/api/';

		$http.get(url + 'books')
		.success(function (data) {
			console.log(data);
		}).error(function (error) {
			console.log(error);
		});

}]);