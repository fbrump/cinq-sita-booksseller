"use strict";

angular.module('BooksService', ['ngResource'])
.factory('resourceBooks', ['$resource',
	function ($resource) {
		return $resource('api/books/:bookId', null, {
			update: {
				method: 'PUT'
			}
		});
}])
.factory('apiBooks', ['resourceBooks', '$q', function (resourceBooks, $q) {
	var service = {};

	service.update = function (book) {
		return $q(function (resolve, reject) {
			if (book._id){
				resourceBooks.update({
					bookId: book._id
				}, book, function () {
					resolve({
						message: 'Book ' + book.name + ' updated succesefuly!',
						inclusion: false
					});
				}, function (error) {
					console.error(error);
					reject({
						message: 'Happened some error when updated booke ' + book.name
					});
				});
			}
		})
	}

	return service;
}])