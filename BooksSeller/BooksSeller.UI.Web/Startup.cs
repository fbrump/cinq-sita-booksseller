﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BooksSeller.UI.Web.Startup))]
namespace BooksSeller.UI.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
