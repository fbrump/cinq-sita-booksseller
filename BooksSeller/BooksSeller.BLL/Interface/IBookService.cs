﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BooksSeller.BE.DTO;

namespace BooksSeller.BLL.Interface
{
    /// <summary>
    /// Interface that have assin of all methods service.
    /// </summary>
    public interface IBookService
    {
        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns>BooksDTO</returns>
        IEnumerable<BookDTO> GetAll();

        /// <summary>
        /// Gets the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Object</returns>
        BookDTO Get(int id);

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>Book updated.</returns>
        BookDTO Update(BookDTO entity);
    }
}
