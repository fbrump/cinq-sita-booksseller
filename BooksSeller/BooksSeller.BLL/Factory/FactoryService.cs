﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksSeller.BLL.Factory
{
    public static class FactoryService<T>
    {
        private static Dictionary<Type, object> _dic;

        /// <summary>
        /// Initializes the <see cref="FactoryService{T}"/> class.
        /// </summary>
        static FactoryService()
        {
            fullDictonaryServices();
        }

        /// <summary>
        /// Gets the implementation.
        /// </summary>
        /// <returns></returns>
        public static T GetImplementation()
        {
            var instance = new object();
            var typeIntreface = typeof (T);

            if (_dic.ContainsKey(typeIntreface))
            {
                instance = _dic[typeIntreface];
            }

            return (T) instance;
        }

        /// <summary>
        /// Fulls the dictonary services.
        /// </summary>
        private static void fullDictonaryServices()
        {
            _dic = new Dictionary<Type, object>()
            {
                { typeof(Interface.IBookService), new Service.BookService() }
            };
        }
    }
}
