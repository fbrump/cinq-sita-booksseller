﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BooksSeller.BE;
using BooksSeller.BE.DTO;
using BooksSeller.BLL.Interface;
using BooksSeller.DAL.Factory;
using BooksSeller.DAL.Interface;

namespace BooksSeller.BLL.Service
{
    /// <summary>
    /// Class that container all business fo the book.
    /// </summary>
    /// <seealso cref="BooksSeller.BLL.Interface.IBookService" />
    internal class BookService : IBookService
    {
        #region [ Repository]
        /// <summary>
        /// The repository book.
        /// </summary>
        private IBookRepository _repositoryBook;
        /// <summary>
        /// Gets the repository book.
        /// </summary>
        /// <value>
        /// The repository book.
        /// </value>
        internal IBookRepository RepositoryBook
        {
            get
            {
                this._repositoryBook = this._repositoryBook ?? FactoryRepository<IBookRepository>.GetImplementation();
                return this._repositoryBook;
            }
        }
        #endregion

        #region [Constructors]

        /// <summary>
        /// Initializes a new instance of the <see cref="BookService"/> class.
        /// </summary>
        public BookService()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BookService"/> class.
        /// </summary>
        /// <param name="iRepositoy">The i repositoy.</param>
        public BookService(IBookRepository iRepositoy)
        {
            this._repositoryBook = iRepositoy;
        }
        #endregion

        #region [ Services ]
        /// <summary>
        /// Gets the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// Object
        /// </returns>
        public BookDTO Get(int id)
        {
            return this.ConvertToDTO(this.RepositoryBook.Get(new BookEntity() { id = id }));
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns>
        /// BooksDTO
        /// </returns>
        public IEnumerable<BookDTO> GetAll()
        {
            var _result = new List<BookDTO>();

            var lst = this.RepositoryBook.GetAll()
                .ToList();
            if (lst.Count > 0)
                lst.ForEach(t => _result.Add(ConvertToDTO(t)));

            return _result;
        }

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Book updated.
        /// </returns>
        public BookDTO Update(BookDTO entity) => ConvertToDTO(RepositoryBook.Update(ConvertToEntity(entity)));
        #endregion

        #region [ Private Methods ]

        private BookDTO ConvertToDTO(BookEntity entity)
        {
            return new BookDTO()
            {
                id = entity.id,
                name = entity.name,
                description = entity.description
            };
        }

        private BookEntity ConvertToEntity(BookDTO dto)
        {
            return new BookEntity()
            {
                id = dto.id,
                name = dto.name,
                description = dto.description
            };
        }

        #endregion
    }
}
