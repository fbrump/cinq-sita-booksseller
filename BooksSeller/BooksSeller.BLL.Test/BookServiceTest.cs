﻿using System;
using System.Linq;
using BooksSeller.BE.DTO;
using BooksSeller.BLL.Factory;
using BooksSeller.BLL.Interface;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BooksSeller.BLL.Test
{
    /// <summary>
    /// Tests of the Book Service.
    /// </summary>
    [TestClass]
    public class BookServiceTest
    {
        [TestMethod]
        public void should_list_all_books()
        {
            var _service = FactoryService<IBookService>.GetImplementation();

            var _result = _service.GetAll();

            Assert.IsNotNull(_result, "The list of book, not come null");
            Assert.AreNotEqual(_result.Count(), 0, "The list haven't records");
        }

        [TestMethod]
        public void should_update_item()
        {
            var _service = FactoryService<IBookService>.GetImplementation();
            var _update = new BookDTO()
            {
                id = 1,
                description = "A bio of the CEO Big Company"
            };
            _update.name = _service.Get(_update.id).name;

            var _result = _service.Update(_update);

            Assert.IsNotNull(_result, "The object returned can't null");
            Assert.AreEqual(_update.description, _result.description, "The item not updated!");
        }
    }
}
