﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksSeller.DAL.Factory
{
    /// <summary>
    /// Factory that work with Interfaces and Implementations of Repository models.
    /// </summary>
    /// <typeparam name="T">Repository Interface.</typeparam>
    public static class FactoryRepository<T>
    {
        private static Dictionary<Type, object> _dic;

        /// <summary>
        /// Initializes the <see cref="FactoryRepository{T}"/> class.
        /// </summary>
        static FactoryRepository()
        {
            fullDictonaryRepostiroy();
        }

        /// <summary>
        /// Gets the implementation.
        /// </summary>
        /// <returns>Instance of the Interface objetct.</returns>
        public static T GetImplementation()
        {
            var instance = new object();
            var typeIntreface = typeof(T);

            if (_dic.ContainsKey(typeIntreface))
            {
                instance = _dic[typeIntreface];
            }

            return (T)instance;
        }

        /// <summary>
        /// Fulls the dictonary repostiroy.
        /// </summary>
        private static void fullDictonaryRepostiroy()
        {
            _dic = new Dictionary<Type, object>()
            {
                { typeof(Interface.IBookRepository), new Repository.BookRepository() }
            };
        }
    }
}
