﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BooksSeller.BE;

namespace BooksSeller.DAL.Interface
{
    /// <summary>
    /// Interface of the Book repository.
    /// </summary>
    public interface IBookRepository
    {
        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        IEnumerable<BookEntity> GetAll();

        /// <summary>
        /// Gets the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        BookEntity Get(BookEntity entity);

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        BookEntity Update(BookEntity entity);

    }
}
