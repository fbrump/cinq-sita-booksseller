﻿using BooksSeller.BE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BooksSeller.DAL.Interface;

namespace BooksSeller.DAL.Repository
{
    /// <summary>
    /// Class that implement Interface of the Book repository.
    /// </summary>
    /// <seealso cref="BooksSeller.DAL.Interface.IBookRepository" />
    internal class BookRepository : IBookRepository
    {
        #region [ Context ]
        /// <summary>
        /// The database book
        /// </summary>
        internal IList<BookEntity> DbBook
        {
            get { return this._dbBook = this._dbBook ?? this.fullDataBase(); }
        }

        private IList<BookEntity> _dbBook;

        #endregion

        #region [ Repository Methods]


        public IEnumerable<BookEntity> GetAll() => DbBook;

        public BookEntity Get(BookEntity entity) => DbBook.SingleOrDefault(t => t.id == entity.id);

        public BookEntity Update(BookEntity entity)
        {
            var _toUpdate = this.Get(entity);

            _toUpdate.name = entity.name;
            _toUpdate.description = entity.description;

            return _toUpdate;
        }
        #endregion

        #region [ Private Methods ]        
        /// <summary>
        /// Fulls the data base.
        /// </summary>
        /// <returns></returns>
        private IList<BookEntity> fullDataBase()
        {
            return new List<BookEntity>()
            {
                { new BookEntity() { id = 1, name = "Steve Jobs"} },
                { new BookEntity() { id = 2, name = "The Children of Húrin"} },
                { new BookEntity() { id = 3, name = "Bible"} },
                { new BookEntity() { id=4, name = "The Sumarillion"} },
            };
        }
        #endregion
    }
}
